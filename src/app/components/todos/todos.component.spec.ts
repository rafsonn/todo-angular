import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Todo } from 'src/app/models/Todo';

import { TodosComponent } from './todos.component';

describe('TodosComponent', () => {
  let component: TodosComponent;
  let fixture: ComponentFixture<TodosComponent>;
  let TODOS: Todo[];

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TodosComponent ]
    })
      .compileComponents();
    TODOS = [
      { content: 'First', completed: false },
      { content: 'Second', completed: false },
      { content: 'Third', completed: false },
    ]
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TodosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('#removeTodo(1) should remove second todo from #todos', () => {
    const comp = new TodosComponent();
    comp.todos = TODOS.slice();
    comp.removeTodo(1);
    expect(comp.todos).toEqual([TODOS[0], TODOS[2]], 'second todo removed');
  })

  it('#toggleAll() should toggle #all', () => {
    const comp = new TodosComponent();
    expect(comp.all).toBe(true, 'all at first');
    comp.toggleAll();
    expect(comp.all).toBe(false, 'completed after click');
    comp.toggleAll();
    expect(comp.all).toBe(true, 'all after second click');
  })

  it('#toggleComplition(2) should toggle complition of the third todo from #todos', () => {
    const comp = new TodosComponent();
    comp.todos = TODOS.slice();
    let index = 2;
    expect(comp.todos[index].completed).toBe(false, 'uncompleted at first');
    comp.toggleComplition(index);
    expect(comp.todos[index].completed).toBe(true, 'completed after click');
    comp.toggleComplition(index);
    expect(comp.todos[index].completed).toBe(false, 'uncompleted after second click');
  })

  it('#addTodo() should add todo to #todos', () => {
    const comp = new TodosComponent();
    
    expect(comp.todos).toEqual([], 'empty at first');
    comp.inputTodo = TODOS[0].content;
    comp.addTodo();
    expect(comp.todos).toEqual([TODOS[0]], 'one todo after first click');
    comp.inputTodo = TODOS[1].content;
    comp.addTodo();
    expect(comp.todos).toEqual([TODOS[0], TODOS[1]], 'two todos after second click');
  })
});
