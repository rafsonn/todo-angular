import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Todo } from 'src/app/models/Todo';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.sass']
})
export class TodoComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  @Output() newTodoEvent = new EventEmitter<number>();
  @Input() todo: Todo = new Todo;
  @Input() id!: number;

  deleteTodo(id: number) {
    this.newTodoEvent.emit(id);
  }

}
