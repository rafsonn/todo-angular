import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TodoComponent } from './todo.component';

describe('TodoComponent', () => {
  let component: TodoComponent;
  let fixture: ComponentFixture<TodoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TodoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TodoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('raises the newTodoEvent event when clicked', () => {
    const comp = new TodoComponent();
    
    spyOn(comp.newTodoEvent, 'emit');
    comp.deleteTodo(2);
    expect(comp.newTodoEvent.emit).toHaveBeenCalledWith(2);
  });
});
