import { Component, OnInit } from '@angular/core';
import { Todo } from './../../models/Todo';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.sass']
})
export class TodosComponent implements OnInit {

  todos: Todo[] = [];
  inputTodo: string = "";
  all:boolean = true;

  constructor() { }

  ngOnInit(): void {
  }

  addTodo() {
    this.todos.push({
      content: this.inputTodo,
      completed: false
    })

    this.inputTodo = "";
  }

  removeTodo(id: number) {
    this.todos.splice(id, 1);
  }

  toggleComplition(id: number) {
    this.todos.map((todo, index) => {
      if (index == id)
        todo.completed = !todo.completed;
      return todo;
    })
  }
  toggleAll() {
    this.all = !this.all;
  }
}
